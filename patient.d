import std.random;
import std.algorithm.comparison;
import medication;
import constants;
import drawable;
import main;
import allegro5.allegro;
import std.math;
import diarrhoea;
import explosion;
import game;
import tutorial;

class Patient : Drawable {
	float lastX, lastY;
	float targetX, targetY;

	enum minConstipation = -maxSeverity, maxConstipation = maxSeverity;
	int constipation;

	enum minTemperature = -maxSeverity, maxTemperature = maxSeverity;
	int temperature;

	enum minInfection = 0, maxInfection = maxSeverity;
	int infection;

	Medication allergyTrigger;
	Medication allergyConsequence;

	Medication prescription;
	int prescriptionCountdown;

	int diarrhoeaCounter;

	int currentSneezeCounter;
	int nextSneezeCounter;

	int blobbiness;

	int diarrhoeaInteractionCounter;
	int contagiousConstipationInteractionCounter;
	int contagiousTemperatureInteractionCounter;
	int contagiousInfectionInteractionCounter;

	int fireCounter;

	//Returns true if the condition being treated was cured (though other conditions may exist).
	bool applyPrescription() {
		if (prescription != Medication.None) {
			bool cured = applyPrescriptionOrSideEffect(prescription);
			if (prescription == allergyTrigger) {
				applyPrescriptionOrSideEffect(allergyConsequence);
			}
			return cured;
		}
		return isHealthy();
	}

	//Returns true if the condition being treated was cured (though other conditions may exist).
	bool applyPrescriptionOrSideEffect(Medication prescription, int strength = 1) {
		final switch (prescription) {
			case Medication.None: return isHealthy();
			case Medication.Antipyretic: temperature = max(temperature - strength, minTemperature); return temperature == 0;
			case Medication.Propyretic: temperature = min(temperature + strength, maxTemperature); return temperature == 0;
			case Medication.Laxative: constipation = max(constipation - strength, minConstipation); return constipation == 0;
			case Medication.Binder: constipation = min(constipation + strength, maxConstipation); return constipation == 0;
			case Medication.Antiblobotics: infection = max(infection - strength, minInfection); return infection == 0;
			case Medication.Problobotics: infection = min(infection + strength, maxInfection); return infection == 0;
		}
	}

	void startPrescription(Medication prescription) {
		assert(prescription != Medication.None);
		this.prescription = prescription;
		prescriptionCountdown = prescriptionLength * prescriptionInterval;
	}

	void tickPrescription(int ticks) {
		if (prescriptionCountdown > 0) {
			int oldNumLeft = numDosesLeft();
			prescriptionCountdown -= ticks;
			if (prescriptionCountdown < prescriptionInterval) prescriptionCountdown = 0;
			int newNumLeft = numDosesLeft();
			for (int i = oldNumLeft; i > newNumLeft; i--) {
				if (applyPrescription()) {
					//Condition being treated was cured! Stop taking the prescription.
					prescriptionCountdown = 0;
					break;
				}
			}
			if (prescriptionCountdown == 0) prescription = Medication.None;
		}
	}

	int numDosesLeft() {
		return prescriptionCountdown / prescriptionInterval;
	}

	void cancelPrescription() {
		prescription = Medication.None;
		prescriptionCountdown = 0;
	}

	void chooseTarget() {
		targetX = uniform!"[]"(minX, maxX);
		targetY = uniform!"[]"(minY, maxY);
	}

	bool isHealthy() {
		return !temperature && !constipation && !infection;
	}

	//Slight misnomer - also returns true when cured and dancing.
	bool isAgitated() {
		return
			prescription == Medication.None ||
			abs(temperature) == maxSeverity ||
			abs(constipation) == maxSeverity ||
			infection == maxSeverity;
	}

	//Retunrs true if the patient should still exist.
	bool update(int numTicks) {
		tickPrescription(numTicks);

		float tx = targetX - x;
		float ty = targetY - y;
		float tmag = sqrt(tx*tx + ty*ty);
		//No movement during a sneeze!
		if (!currentSneezeCounter) {
			float dx = x - lastX;
			float dy = y - lastY;
			float dmag = sqrt(dx*dx + dy*dy);
			if (dmag < 0.01) {
				float a = uniform!"[)"(0, 2*3.14159);
				dx = sin(a);
				dy = cos(a);
			} else {
				dx /= dmag;
				dy /= dmag;
			}
			//Make sure they turn round if they wander into the door by accident
			if (dy > 0 && !isHealthy() && y > maxY-patientRadius) {
				float maxDy = 1 - (y - (maxY-patientRadius)) / patientRadius;
				if (dy > maxDy) dy = max(-dy, maxDy);
			}
			if (tmag < 0.01) {
				float a = uniform!"[)"(0, 2*3.14159);
				tx = sin(a);
				ty = cos(a);
			} else {
				tx /= tmag;
				ty /= tmag;
			}
			dx += (tx - dx) * patientAccel * numTicks;
			dy += (ty - dy) * patientAccel * numTicks;
			dmag = sqrt(dx*dx+dy*dy);
			auto patientSpeed = (isAgitated() ? patientSpeedAgitated : patientSpeedPrescribed);
			if (dmag < 0.01) {
				float a = uniform!"[)"(0, 2*3.14159);
				dx = patientSpeed * sin(a);
				dy = patientSpeed * cos(a);
			} else {
				dx *= patientSpeed / dmag;
				dy *= patientSpeed / dmag;
			}
			lastX = x;
			lastY = y;
			x += dx * numTicks;
			y += dy * numTicks;
		}

		x = clamp(x, minX+patientRadius, maxX-patientRadius);
		if (y < minY+patientRadius) y = minY+patientRadius;

		//Resolve collisions against the two outward corners next to the door
		void collideWithDoor(float xBoundary, float xReset) {
			if (y >= maxY) {
				x = xReset;
			} else {
				float dx = x - xBoundary;
				float dy = y - maxY;
				float dmag = sqrt(dx*dx+dy*dy);
				if (dmag < patientRadius) {
					x = xBoundary + dx * patientRadius / dmag;
					y = maxY + dy * patientRadius / dmag;
				}
			}
		}
		if (x <= minXExiting || x >= maxXExiting) {
			y = clamp(y, minY+patientRadius, maxY-patientRadius);
		} else if (x < minXExiting+patientRadius) {
			collideWithDoor(minXExiting, minXExiting+patientRadius);
		} else if (x > maxXExiting-patientRadius) {
			collideWithDoor(maxXExiting, maxXExiting-patientRadius);
		}

		if (isHealthy()) {
			targetX = (minXExiting + maxXExiting) * 0.5;
			targetY = maxYExiting;
			if (y >= maxYExiting) {
				theGame.createPatient();
				theGame.createPatient();
				theTutorial.onPatientsReferred();
				theGame.awardPoints(pointsForCuring);
				return false;
			}
		} else {
			if (tmag < patientNewTargetThreshold || targetY > maxY) {
				chooseTarget();
			}
		}

		diarrhoeaCounter = max(diarrhoeaCounter - numTicks, 0);
		if (diarrhoeaCounter == 0 && constipation == minConstipation) {
			auto diarrhoea = new Diarrhoea();
			diarrhoea.x = x;
			diarrhoea.y = y;
			theGame.diarrhoeas ~= diarrhoea;
			diarrhoeaCounter = diarrhoeaInterval;
			playSample(sfx.diarrhoeas[uniform!"[)"(0,$)], 0.2);
		}

		if (constipation == maxConstipation) {
			auto explosion = new Explosion();
			explosion.isPrimary = true;
			explosion.x = x;
			explosion.y = y;
			theGame.explosions ~= explosion;
			explosion.createDiarrhoea();
			theGame.awardPoints(pointsForKilling);
			playSample(sfx.burst, 1);
			return false;
		}

		currentSneezeCounter = max(currentSneezeCounter - numTicks, 0);
		nextSneezeCounter = max(nextSneezeCounter - numTicks, 0);
		if (nextSneezeCounter == 0 && blobbiness == 0 && infection) {
			currentSneezeCounter = sneezeLength;
			nextSneezeCounter = cast(int)round(maxSneezeInterval * pow(minSneezeInterval / cast(float)maxSneezeInterval, infection / cast(float)maxInfection));
			playSample(sfx.sneeze, 0.5);
		}

		if (infection == maxInfection) {
			if (currentSneezeCounter == 0) {
				blobbiness = min(blobbiness + numTicks, blobbinessMorphFrames);
			}
		} else {
			blobbiness = max(blobbiness - numTicks, 0);
		}

		diarrhoeaInteractionCounter = max(diarrhoeaInteractionCounter - numTicks, 0);
		contagiousConstipationInteractionCounter = max(contagiousConstipationInteractionCounter - numTicks, 0);
		contagiousInfectionInteractionCounter = max(contagiousInfectionInteractionCounter - numTicks, 0);
		contagiousTemperatureInteractionCounter = max(contagiousTemperatureInteractionCounter - numTicks, 0);

		if (fireCounter > 0) {
			fireCounter -= numTicks;
			if (fireCounter <= 0) {
				auto explosion = new Explosion();
				explosion.isPrimary = false;
				explosion.x = x;
				explosion.y = y;
				theGame.explosions ~= explosion;
				theGame.awardPoints(pointsForKilling);
				playSample(sfx.burst, 0.5);
				return false;
			}
		}

		return true;
	}

	override void draw() {
		//Choose a head
		auto head = gfx.healthy;
		int severity = 0;
		if (infection > severity) head = morphers.infected.frames[severity = infection];
		if (temperature < -severity) head = morphers.chills.frames[severity = -temperature];
		if (temperature > severity) head = morphers.fever.frames[severity = temperature];
		if (constipation < -severity) head = morphers.diarrhoea.frames[severity = -constipation];
		if (constipation > severity) head = morphers.constipated.frames[severity = constipation];
		if (currentSneezeCounter > 0) {
			int frame = currentSneezeCounter;
			if (frame > sneezeMorphFrames) {
				frame = sneezeLength - frame;
				if (frame > sneezeMorphFrames) {
					frame = sneezeMorphFrames;
				}
			}
			head = morphers.sneeze.frames[frame];
		}
		//Choose a body and animation
		if (isHealthy()) {
			int frame = globalTicks % (danceMorphFrames * 4);
			int danceMorpher = frame / (danceMorphFrames * 2);
			frame %= danceMorphFrames * 2;
			if (frame > danceMorphFrames) frame = danceMorphFrames * 2 - frame;
			auto dance = morphers.dances[danceMorpher].frames[frame];
			int w = al_get_bitmap_width(dance);
			int h = al_get_bitmap_height(dance);
			float bob = 8 * cos(frame * 3.14159 / danceMorphFrames);
			float ytop = y - bob;
			float ybottom = y;
			if (bob > 0) ybottom -= bob;
			enum heightFactor = 0.88;
			al_draw_scaled_bitmap(dance, 0, 0, w, h, x - w/2, ytop - heightFactor*h, w, ybottom - ytop + heightFactor*h, 0);
			al_draw_bitmap(head, x - al_get_bitmap_width(head) / 2, (ytop + ybottom) * 0.5 - patientHeight, 0);
		} else if (blobbiness == 0) {
			int frame = globalTicks;
			//Patients waiting for a prescription are more agitated.
			if (isAgitated()) frame *= 2;
			frame %= (walkMorphFrames * 2);
			if (frame > walkMorphFrames) frame = walkMorphFrames * 2 - frame;
			auto walk = morphers.walk.frames[frame];
			al_draw_bitmap(walk, x - al_get_bitmap_width(walk) / 2, y - al_get_bitmap_height(walk), 0);
			al_draw_bitmap(head, x - al_get_bitmap_width(head) / 2, y - patientHeight, 0);
		} else if (blobbiness == blobbinessMorphFrames) {
			int frame = globalTicks % (blobWalkMorphFrames * 2);
			if (frame > blobWalkMorphFrames) frame = blobWalkMorphFrames * 2 - frame;
			auto blobWalk = morphers.blobWalk.frames[frame];
			al_draw_bitmap(blobWalk, x - al_get_bitmap_width(blobWalk) / 2, y - al_get_bitmap_height(blobWalk), 0);
		} else {
			auto partBlob = morphers.blobbiness.frames[blobbiness];
			al_draw_bitmap(partBlob, x - al_get_bitmap_width(partBlob) / 2, y - al_get_bitmap_height(partBlob), 0);
			int hw = al_get_bitmap_width(head);
			int hh = al_get_bitmap_height(head);
			float shrink = blobbiness / cast(float)blobbinessMorphFrames;
			float size = 1-shrink;
			al_draw_scaled_bitmap(head, 0, 0, hw, hh, x - hw*size/2, y - patientHeight + hh*shrink, hw*size, hh*size, 0);
		}
		//Overlay fire if applicable.
		if (fireCounter) {
			float t = 1 - fireCounter / cast(float)patientFireLifetime;
			int frame = (patientFireLifetime - fireCounter) % (fireMorphFrames * 2);
			if (frame > fireMorphFrames) frame = fireMorphFrames*2 - frame;
			auto fire = morphers.fire.frames[frame];
			int w = al_get_bitmap_width(fire);
			int h = al_get_bitmap_height(fire);
			al_draw_scaled_bitmap(fire, 0, 0, w, h, x - w*t/2, y - patientHeight*1.3*t, w*t, patientHeight*1.3*t, 0);
			addFireSfx(t * 0.1);
		}
	}
}
