import main;
import allegro5.allegro;
import allegro5.allegro_font;

void drawTitleScreen(int progress, int maxProgress) {
	al_clear_to_color(al_map_rgb(0,0,0));
	int y = 80;
	auto white = al_map_rgb(255,255,255);
	auto green = al_map_rgb(144,255,128);
	al_draw_text(theFont, white, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `Dr Havoc Shitfan`);
	al_draw_text(theFont, white, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `© 2017 Ben 'Bruce "entheh" Perry' Wieczorek-Davis`);
	y += fontHeight;
	al_draw_text(theFont, white, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `An entry into the TINS 2017 Allegro game programming competition`);
	al_draw_text(theFont, white, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `tins.amarillion.org/2017`);
	y += fontHeight;
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `The font is Brushaff by Situjuh Nazara`);
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `Please see enclosed font directory for details`);
	y += fontHeight;
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `All code was written in D with SiegeLord's Allegro 5 bindings`);
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `All graphics drawn on a Cintiq 24HD with Corel Painter 2017`);
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `Music made in OpenMPT`);
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `Sound effects recorded with a Zoom H2n`);
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `All in a 72-hour weekend`);
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `Fire sound effect re-used from my SpeedHack 2014 game 'Badonkadonk!'`);
	y += fontHeight;
	al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `~ ~ ~`);
	y += fontHeight;
	if (progress < maxProgress) {
		al_draw_textf(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `Preparing morphing effects: %d%% complete`, (progress * 100 + maxProgress/2) / maxProgress);
	} else {
		al_draw_text(theFont, green, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `Morphing effects ready`);
		y += fontHeight;
		al_draw_text(theFont, white, 400, y += fontHeight, ALLEGRO_ALIGN_CENTRE, `Click to start!`);
	}
	al_flip_display();
}
