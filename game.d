import std.math;
import std.algorithm.comparison;
import std.algorithm.sorting;
import std.random;
import patient;
import constants;
import medication;
import patientpopup;
import allegro5.allegro;
import diarrhoea;
import explosion;
import drawable;
import interaction;
import tutorial;

Game theGame;

class Game {
	int score;
	int timeLeft = 5*60*FPS;

	Patient[] patients;
	Diarrhoea[] diarrhoeas;
	Explosion[] explosions;
	PatientPopup patientPopup;

	Drawable[] reusableDrawables;

	void populateReusableDrawables() {
		reusableDrawables.length = 0;
		reusableDrawables ~= patients;
		reusableDrawables ~= diarrhoeas;
		reusableDrawables ~= explosions;
	}

	this() {
		foreach (i; 0..numStartingPatients) {
			createPatient();
		}
	}

	void createPatient() {
		auto patient = new Patient();

		patient.x = uniform!"[]"(minXExiting, maxXExiting);
		patient.y = maxYExiting;
		patient.lastX = patient.x;
		patient.lastY = patient.y + 1;
		patient.chooseTarget();

		patient.allergyTrigger = randomMedication();
		patient.allergyConsequence = randomAllergyConsequence(patient.allergyTrigger);

		auto ailment = randomAilment();
		int severity = uniform!"[]"(minPresentingSeverity, maxPresentingSeverity);
		patient.applyPrescriptionOrSideEffect(ailment, severity);

		patients ~= patient;
	}

	void update(int numTicks) {
		if (patientPopup) {
			if (patientPopup.update(numTicks)) {
				patientPopup = null;
			}
			return;
		}

		//Sort everything by x position for overlap resolution
		populateReusableDrawables();
		sort!"a.x < b.x"(reusableDrawables);

		//Resolve overlaps
		int first = 0;
		Interaction interaction;
		interaction.numTicks = numTicks;
		foreach (last, lastDrawable; reusableDrawables) {
			while (first < last && lastDrawable.x - reusableDrawables[first].x >= InteractionDistances.max) {
				first++;
			}
			foreach (midDrawable; reusableDrawables[first..last]) {
				if (midDrawable.destroyedDuringInteraction) continue;
				with (interaction) {
					dx = lastDrawable.x - midDrawable.x;
					dy = lastDrawable.y - midDrawable.y;
					dist = sqrt(dx*dx + dy*dy);
					if (dist < InteractionDistances.max) {
						interact(lastDrawable, midDrawable);
						if (lastDrawable.destroyedDuringInteraction) {
							break;
						}
					}
				}
			}
		}

		//Update patients. Careful, array can get appended to during the loop!
		//I have no idea whether foreach would cope with that, so use a traditional for loop.
		int writePos = 0;
		for (int readPos = 0; readPos < patients.length; readPos++) {
			auto patient = patients[readPos];
			if (!patient.destroyedDuringInteraction && patient.update(numTicks)) {
				patients[writePos++] = patient;
			}
		}
		patients.length = writePos;
		if (patients.length == 0) theTutorial.onNoPatientsLeft();
		if (patients.length >= numStartingPatients + 5) theTutorial.onGameWellUnderWay();

		writePos = 0;
		foreach (readPos, diarrhoea; diarrhoeas) {
			if (!diarrhoea.destroyedDuringInteraction) {
				//Limit the total amount of diarrhoea in the map - the oldest spontaneously catches fire.
				if (diarrhoeas.length - readPos > maxDiarrhoeas && !diarrhoea.fireLifetime) {
					diarrhoea.fireLifetime = diarrhoeaFireLifetime;
				}
				if (diarrhoea.update(numTicks)) {
					diarrhoeas[writePos++] = diarrhoea;
				}
			}
		}
		diarrhoeas.length = writePos;

		writePos = 0;
		foreach (readPos, explosion; explosions) {
			if (!explosion.destroyedDuringInteraction) {
				if (explosion.update(numTicks)) {
					explosions[writePos++] = explosion;
				}
			}
		}
		explosions.length = writePos;

		if (!theTutorial.gameIsOver()) {
			timeLeft -= numTicks;
			if (timeLeft <= 0) {
				timeLeft = 0;
				theTutorial.onTimeUp();
			}
		}
	}

	Patient getPatientUnderMouse(int x, int y) {
		Patient bestMatch = null;
		float bestDistanceSq = 1e+20;
		foreach (patient; patients) {
			float dx = x - patient.x;
			float dy = y - patient.y;
			if (abs(dx) < patientRadius && dy >= -patientHeight && dy < 0) {
				//For distance comparison, centre on the upper part of the patient (it's most visible).
				dy += patientHeight*0.75;
				//Make x more important when comparing distance, since patients are tall.
				//dx *= patientHeight*0.5 / patientRadius;
				float distanceSq = dx*dx + dy*dy;
				if (distanceSq < bestDistanceSq) {
					bestMatch = patient;
					bestDistanceSq = distanceSq;
				}
			}
		}
		return bestMatch;
	}

	void onMouseDown(ref ALLEGRO_EVENT event) {
		if (patientPopup && !patientPopup.isFinishing()) {
			patientPopup.onMouseDown(event);
			return;
		}

		//Who did we click on?
		auto clickedPatient = getPatientUnderMouse(event.mouse.x, event.mouse.y);
		if (clickedPatient) {
			patientPopup = new PatientPopup(clickedPatient);
			theTutorial.onClickedPatient();
		}
	}

	void awardPoints(int points) {
		if (!theTutorial.gameIsOver()) {
			score += points;
		}
	}
}
