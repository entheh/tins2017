# Idea

[Note: this idea file was made at the start, and doesn't necessarily match up with what the game became.]

You are a rogue doctor. Your goal is to cause as much damage as possible.

Making a patient better causes them to recommend two more patients.
This can be used to build up your stock of patients to treat badly.

Treating a patient badly causes their demise in a number of interesting ways.

Patients have stats:
- Stools: constipated, normal, incontinent
- Temperature: fever, normal, chills
- Infection level (leads to sneezing)
- Allergies: per patient, certain medications might have side-effects to other stats

Medications are available for prescription:
- Something to raise temperature
- Something to lower temperature
- Laxative
- Binder
- Antiblobotics to cure infection
- Problobotics to increase infection

If sufficiently incontinent, a trail of diarrhoea is left where the patient walks. This is flammable.
If sufficiently constipated, a patient will explode.
If sufficiently hot, a patient will catch fire and set fire to any diarrhoea.
If sufficiently cold, a patient can put fire out.
Sneezing patients can inflict their non-infectious ailments on other patients nearby.
A sufficient level of infection leads to a patient morphing into a blob, which can infect other patients nearby.

Your inventory of medications is finite but steadily replenished.

A game lasts a finite time (maybe three minutes?) and produces a score based somehow on mayhem caused.
Once patients start noticing ill effects (PFPFFFAHAHUPAFPRFPFURTLE), they will report you and you will get arrested.

You can prescribe things for patients by clicking on the patients.
Maybe the game pauses while you're prescribing / changing prescriptions, so you can see the dancing then.
