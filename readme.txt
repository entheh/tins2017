Dr Havoc Shitfan
TINS Allegro Game Programming Competition 2017
tins.amarillion.org/2017
Ben 'Bruce "entheh" Perry' Wieczorek-Davis
ben@wieczorekdavis.com


Compiling
---------

I've written my entry in D this time. D is a wonderful language which offers
a lot of the safety you get from Java or C# - things like garbage collection,
bounds checking, everything initialised to a default value - while still
compiling to native. Also, the deeper I get into it, the more fantastically
good design decisions I find - global variables being thread-local by
default, type syntax like const(char)* being less confusing ...

However, that does mean it might be one of the less trivial entries to
compile.

The blurb above and the compilation instructions below are largely copied and
pasted from my TINS 2012 entry, and it's a while since I actually set D up.
So I hope the "bare minimum of instructions" I wrote last time will be
sufficient again!

I have included a Windows executable. If you downloaded from the TINS site
where there is a size limit, you will need to obtain the DLLs separately.
Otherwise at the time of writing you can get a zip including DLLs from:

http://bdavis.strangesoft.net/DrHavocShitfan.zip


Licence
-------

- You may distribute this game unmodified, or with minimal portability fixes,
  or with an added executable, or both - provided that you do it strictly for
  non-profit purposes.

- You may of course enjoy the game to your heart's content.

- All other rights are reserved.


Compiling the game
------------------

You will need Allegro 5.2, and you will need it built with libpng, freetype
and DUMB. For Windows users, the easiest way to get it these days is via
NuGet in Visual Studio.

You can get D from:

http://dlang.org/

I wrote the game using D 2.071.0. The language does change, so other versions
may require tweaks to the code, but hopefully nothing major.

You will also need to get SiegeLord's D bindings from here:

https://github.com/SiegeLord/DAllegro5

Follow SiegeLord's instructions to get up and running. You'll probably need
to generate D-format 'lib' files that map on to Allegro's DLLs, in addition
to dallegro5.lib which has no corresponding DLL. You might also need other
D dependencies.

D has the concept of 'import' paths - these are directories where other .d
files can be found, sort of. To be precise, if my file says
"import allegro5.allegro;" in it, and you've specified an import path of
"C:\DAllegro5", then the D compiler will need to be able to find
"C:\DAllegro5\allegro5\allegro.d".

D also has library search paths - just specify the directories containing
the lib files you generated for Allegro 5 using D's tools.

You may need to mess around with DAllegro5's 'pragma(lib, "...")' lines -
search for pragma. It depends how your Allegro build is arranged. Or
alternatively you can just specify the required libraries on the command
line. Don't forget to include dallegro5.lib in addition to the Allegro
ones.

Other than that, note that D tends to like to compile everything together.
Specify all the .d files at once.

For Windows, you're best building it as a console application until you've
established that it runs without crashing, as exceptions are logged to the
console. Once it works, I recommend compiling with -L/SUBSYSTEM:WINDOWS:4.0
- this will make it a Windows application instead of a console one.


Rules
-----

*** Genre requirement ***

--- Theme: Doctors & Health ---

You are a doctor in charge of several patients.

On that note, I want to mention that I have not exactly been in great shape
myself. I work for PlayFusion, and they - being the cool company they are -
arranged a Kilimanjaro climb for a group of us this year. After spending a
good half of the year training, buying stuff and generally preparing, I came
down with a persistent infection which I'm still fighting, and couldn't go.
Furthermore, the first antiblobotic I was given was ciprofloxacin, a member
of a group of antiblobotics that can in rare cases cause permanent damage to
tendons, nerves and various other parts of the body. My tendons were slightly
affected and of course I'm now terrified that it's permanent - although it's
not too bad so far.

So, when you read below that I didn't manage to do a font, or you miss my
customary high score table or options menu or whatever - just bear in mind
that this time, I'm the doctor - and I gained the position without having to
swear a Hippocratic Oath. (On the other hand, thanks to me, so did you...)


*** Artistic requirements ***

--- Pause mode where all characters dance to a funky tune ---

Having a patient prescription popup popped up pauses the game. Do you like
my alliteration? All the characters continue to animate, and while some of
them could be said to be quite unenthusiastic, the cured ones will really
show off. I took inspiration from the official music video for Big Fish
Little Fish, available at the time of writing at youtu.be/h1pkalv_sAM .
Why was that fresh in my mind? Because of youtu.be/kx35WN3uLis .
As (again at the time of writing) is true for most of the top commenters:
"Bigclive sent me, big fuse little fuse cardboard box"
"Big fuse Little fuse Big fuse Little fuse Big fuse Little fuse cardboard box"
"Big fuse, little fuse"
"Big Clive's version is better."
"I'm only here because of Bigclive. I regret nothing!"
"And now I'm here from bigclivedotcom"
"big clive little clive chinese crap. :p"
"Big Clive, little Clive, cardboard Clive"
"BigClive sent me."

--- The game must include a silly weapon or powerup ---

Well, one of the medications you can prescribe is pretty much never good
for anyone. Certainly all of them can be used as weapons. I hope that's
good enough.


*** Technical requirement ***

--- Use a morphing effect somewhere in the game ---

This one I'm proud of.

I implemented a morphing engine based on the Lucas-Kanade optical flow
algorithm. What this does is it looks at a patch in each of the source and
destination images. For each pixel that is brighter in one image than the
other, it looks at the gradient direction in that location, and uses that to
infer an estimate for where that pixel might have moved to. These are then
munged together in a spectacular display of matrix maths. There's a little
more to it - patches must be normalised so that brightness comparisons are
valid, it's done iteratively with a sanity check to ensure that the match
quality is actually increasing with each iteration, and it's done on a
low-resolution version of the image first, followed by successively fine
versions (this is an image pyramid).

Normally this would be applied at interesting coordinates such as corners
which are identified using a feature detector. I didn't do this; I just made
a fixed-size grid and applied it at each point of the grid. I then allowed
some influence between grid cells to help keep everything reasonably
consistent from point to point (especially on the coarse pyramid levels).
The result is, shall we say, a bit fudgy - but it's good enough that you can
see that something special is going on.

Although I had randomness in it at first, I replaced that with something
deterministic, so you should see exactly the same morphing effect as I do.
If not, then hey, maybe yours is better!

Nearly all the animations are powered by this morphing engine. The only ones
that aren't are the ones consisting of scaling an image down to zero size or
something like that.


*** Bonus rule ***

--- Act of Youtube ---

I didn't need to opt out of any rules, and the prospect of making a YouTube
video seemed a little too daunting considering how tired I was by the end of
the competition. You'll have to make do with Big Clive's! Or my previous or
future, completely unrelated, composition videos:

https://www.youtube.com/channel/UCGUqU00M89nIZtgvqGWJ87g

Or the silly Farage video that's in there.


Re-use of code and other materials
----------------------------------

I did almost everything from scratch for the competition, with three
exceptions: the font (which isn't mine), the fire sound effect (which I
took from my earlier game 'Badonkadonk!'), and this readme :P (and the
obvious stuff like Allegro).

Ideally a font would have all its digits the same width. I made such a fix
on a previous occasion, but this time I didn't - it didn't seem important
enough this time, although ideally I would.

A few other specific details about how the graphics and audio were made are
included on the title screen when you run the game.


New this year!
--------------

A git repository is available! As I write this, it is at

https://gitlab.com/entheh/tins2017

If I've found out how to rename it, you may find it at

https://gitlab.com/entheh/DrHavocShitfan

Then again, I may have to review gitlab's T&C before taking that risk.
Failing both of those, you could always try

https://gitlab.com/users/entheh/projects

For the git-uninitiated, this is a system that lets you view my progress.
Any version that I decided was good enough to 'commit' into the repository
can be retrieved, reviewed, compiled and run. It was also a useful tool for
me of course, since I could bank changes I liked and then revert to them or
compare against them as I went.
