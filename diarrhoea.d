import drawable;
import main;
import allegro5.allegro;
import constants;
import std.algorithm.comparison;

class Diarrhoea : Drawable {
	int fireLifetime = 0;
	float z = 0, dx = 0, dy = 0, dz = 0;
	bool preventFireSpread;

	//Returns false if destroyed.
	bool update(int numTicks) {
		if (fireLifetime) {
			fireLifetime -= numTicks;
			if (fireLifetime <= 0) {
				return false;
			}
		}
		//Explosions can turn diarrhoea into projectiles briefly (or create it).
		if (z) {
			x += dx * numTicks;
			y += dy * numTicks;
			z += dz * numTicks;
			dz -= gravity * numTicks;
			if (z < 0) z = 0;
			x = clamp(x, minX + diarrhoeaRadius, maxX - diarrhoeaRadius);
			y = clamp(y, minY + diarrhoeaRadius, maxY - diarrhoeaRadius);
		}
		return true;
	}

	override void draw() {
		auto image = gfx.diarrhoea;
		float gain = 0;
		if (fireLifetime) {
			int frame = diarrhoeaFireLifetime - fireLifetime;
			if (frame < diarrhoeaToFireMorphFrames) {
				gain = frame / cast(float)diarrhoeaToFireMorphFrames;
				image = morphers.diarrhoeaToFire.frames[frame];
			} else {
				gain = 1;
				frame -= diarrhoeaToFireMorphFrames;
				frame %= fireMorphFrames*2;
				if (frame > fireMorphFrames) frame = fireMorphFrames*2 - frame;
				image = morphers.fire.frames[frame];
			}
		}
		int w = al_get_bitmap_width(image);
		int h = al_get_bitmap_height(image);
		if (fireLifetime && fireLifetime < diarrhoeaFireShrinkLength) {
			float size = fireLifetime / cast(float)diarrhoeaFireShrinkLength;
			al_draw_scaled_bitmap(image, 0, 0, w, h, x - w*size/2, y - h*size, w*size, h*size, 0);
			gain = size;
		} else {
			al_draw_bitmap(image, x - al_get_bitmap_width(image) / 2, y - al_get_bitmap_height(image), 0);
		}
		addFireSfx(gain * 0.004);
	}
}
