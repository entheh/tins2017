import allegro5.allegro;
import allegro5.allegro_font;
import main;

Tutorial theTutorial;

class Tutorial {
	int step = 0;

	void drawText(const char *text) {
		al_draw_text(theFont, al_map_rgb(0,0,0), 400, 26, ALLEGRO_ALIGN_CENTRE, text);
	}

	void draw() {
		switch (step) {
			case 0:
				drawText("Click on your patients to view their details.");
				break;
			case 1:
				drawText("Treat a patient's condition successfully and he will refer two more patients for you.");
				break;
			case 2:
				drawText("Patients look less agitated once they have their prescription. They take one dose a second, stopping if cured.");
				break;
			case 3:
				drawText("Build up your customer base, then start mistreating them to see what happens!");
				break;
			case 100:
				drawText("No patients left! Click to play again.");
				break;
			case 101:
				drawText("Time's up! Your score is now fixed, but you can still mess with the patients.");
				break;
			default:
				break;
		}
	}

	void onClickedPatient() {
		if (step == 0) step++;
	}

	void onPrescriptionWritten() {
		if (step == 1) step++;
	}

	void onPatientsReferred() {
		if (step == 2) step++;
	}

	void onGameWellUnderWay() {
		if (step == 3) step++;
	}

	void onNoPatientsLeft() {
		step = 100;
	}

	void onTimeUp() {
		if (step < 100) step = 101;
	}

	bool gameIsOver() {
		return step >= 100;
	}

	bool noPatientsLeft() {
		return step == 100;
	}
}
