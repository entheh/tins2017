import allegro5.allegro;
import allegro5.allegro_font;
import main;
import patient;
import std.format;
import std.algorithm.comparison;
import medication;
import std.string;
import std.math;
import game;
import tutorial;
import constants;

/*
UI
+-----------------------------------+
|Healthy! Thank you!    |Prescribe: |
|Will refer two friends!|           |
|/                      |a          |
|1% constipated         |b          |
|1% infected            |c          |
|                       |d          |
|No allergies           |e          |
|/                      |f          |
|Allergy to binders:    |           |
|constipation           |           |
|                       |           |
|Nothing prescribed yet |           |
|/                      |           |
|Current prescription:  |Stop taking|
|x (x doses left)       |           |
+-----------------------------------+
*/

class PatientPopup {
	Patient patient;

	struct Line {
		string text;
		Medication medication;

		this(string text, Medication medication = Medication.None) {
			this.text = text;
			this.medication = medication;
		}
	}

	Line[] leftLines;
	Line[] rightLines;

	enum xMargin0 = 20;
	enum xMargin1 = 8;
	enum xMargin2 = 4;
	enum xMargin3 = 12;
	enum yMargin = 16;

	int leftWidth, rightWidth, height;

	int oldLeftWidth, oldRightWidth, oldHeight;
	int transition;
	enum maxTransition = 10;

	int left, middle, right;
	int top, bottom;

	this(Patient patient) {
		this.patient = patient;
		build();
	}

	void interpolateTransition(out int currentLeftWidth, out int currentRightWidth, out int currentHeight) {
		int t = transition*transition*transition;
		enum maxT = maxTransition*maxTransition*maxTransition;
		currentLeftWidth = leftWidth + (oldLeftWidth - leftWidth) * t / maxT;
		currentRightWidth = rightWidth + (oldRightWidth - rightWidth) * t / maxT;
		currentHeight = height + (oldHeight - height) * t / maxT;
	}

	void startHiding() {
		interpolateTransition(oldLeftWidth, oldRightWidth, oldHeight);
		leftWidth = rightWidth = height = 0;
		transition = maxTransition;
	}

	void build() {
		interpolateTransition(oldLeftWidth, oldRightWidth, oldHeight);
		transition = maxTransition;

		//Left side
		leftLines.length = 0;
		if (patient.isHealthy()) {
			leftLines ~= Line("Healthy! Thank you!");
			leftLines ~= Line("Will refer two friends!");
		} else {
			if (patient.constipation > 0) {
				leftLines ~= Line(format("%d%% constipated", (patient.constipation * 100 + patient.maxConstipation/2) / patient.maxConstipation));
			} else if (patient.constipation < 0) {
				leftLines ~= Line(format("%d%% incontinent", (patient.constipation * 100 + patient.minConstipation/2) / patient.minConstipation));
			}
			if (patient.temperature > 0) {
				leftLines ~= Line(format("%d%% fever", (patient.temperature * 100 + patient.maxTemperature/2) / patient.maxTemperature));
			} else if (patient.temperature < 0) {
				leftLines ~= Line(format("%d%% chills", (patient.temperature * 100 + patient.minTemperature/2) / patient.minTemperature));
			}
			if (patient.infection > 0) {
				leftLines ~= Line(format("%d%% infected", (patient.infection * 100 + patient.maxInfection/2) / patient.maxInfection));
			}
		}
		leftLines ~= Line("");
		leftLines ~= Line(allergyTriggerStrings[patient.allergyTrigger]);
		if (patient.allergyTrigger != Medication.None) {
			leftLines ~= Line(allergyConsequenceStrings[patient.allergyConsequence]);
		}
		leftLines ~= Line("");
		if (patient.prescription == Medication.None) {
			leftLines ~= Line("Nothing prescribed yet");
		} else {
			leftLines ~= Line("Current prescription:");
			auto numDosesLeft = patient.numDosesLeft();
			leftLines ~= Line(format("%s (%d dose%s left)", prescriptionStrings[patient.prescription], numDosesLeft, numDosesLeft == 1 ? "" : "s"));
		}

		//Right side
		rightLines.length = 0;
		rightLines ~= Line("Prescribe:");
		rightLines ~= Line("");
		foreach (medication; cast(Medication)(Medication.None+1)..cast(Medication)(Medication.max+1)) {
			if (medication != patient.prescription) {
				rightLines ~= Line(prescriptionStrings[medication], medication);
			}
		}
		if (patient.prescription != Medication.None) {
			rightLines ~= Line("");
			rightLines ~= Line(format("Stop taking %s", prescriptionStrings[patient.prescription]), patient.prescription);
		}

		int getWidth(Line[] lines) {
			int greatestWidth = 0;
			foreach (ref line; lines) {
				int width = al_get_text_width(theFont, line.text.toStringz());
				greatestWidth = max(width, greatestWidth);
			}
			return greatestWidth;
		}
		leftWidth = getWidth(leftLines);
		rightWidth = getWidth(rightLines);
		//leftWidth = rightWidth = max(leftWidth + rightWidth);
		height = max(leftLines.length, rightLines.length) * fontHeight + 2*yMargin;
	}

	void positionPopup() {
		int leftWidth, rightWidth, height;
		interpolateTransition(leftWidth, rightWidth, height);

		int width = xMargin0 + leftWidth + xMargin1 + xMargin2 + rightWidth + xMargin3;

		if (patient.x >= 400) {
			left = right = cast(int)round(patient.x) + patientRadius;
			left -= width;
			if (left < 0) {
				right -= left;
				left = 0;
			}
		} else {
			left = right = cast(int)round(patient.x) - patientRadius;
			right += width;
			if (right > 800) {
				left += 800 - right;
				right = 800;
			}
		}
		if (patient.y >= 300) {
			top = bottom = cast(int)round(patient.y) - patientHeight;
			top -= height;
			if (top < 0) {
				bottom -= top;
				top = 0;
			}
		} else {
			top = bottom = cast(int)round(patient.y);
			bottom += height;
			if (bottom > 600) {
				top += 600 - bottom;
				bottom = 600;
			}
		}

		middle = left + xMargin0 + leftWidth + xMargin1;
	}

	//Returns true when finished.
	bool update(int numTicks) {
		if (transition > 0) {
			transition -= numTicks;
			if (transition <= 0) {
				transition = 0;
				if (isFinishing()) return true;
			}
			positionPopup();
		}
		return false;
	}

	bool isFinishing() {
		return !leftWidth && !rightWidth && !height;
	}

	void draw(ref ALLEGRO_MOUSE_STATE mouse) {
		al_draw_scaled_bitmap(gfx.prescriptions[0], 0, 0, al_get_bitmap_width(gfx.prescriptions[0]), al_get_bitmap_height(gfx.prescriptions[0]), left, top, middle-left, bottom-top, 0);
		al_draw_scaled_bitmap(gfx.prescriptions[1], 0, 0, al_get_bitmap_width(gfx.prescriptions[1]), al_get_bitmap_height(gfx.prescriptions[1]), middle, top, right-middle, bottom-top, 0);

		if (transition > 0) return;

		Line* lineUnderMouse = (mouse.display ? getLineUnderMouse(mouse.x, mouse.y) : null);

		void drawLines(Line[] lines, float x) {
			float y = top + yMargin;
			foreach (ref line; lines) {
				ALLEGRO_COLOR colour = (&line == lineUnderMouse ? al_map_rgb(0,128,0) : al_map_rgb(0,0,0));
				al_draw_text(theFont, colour, x, y, ALLEGRO_ALIGN_LEFT, line.text.toStringz());
				y += fontHeight;
			}
		}
		drawLines(leftLines, left + xMargin0);
		drawLines(rightLines, middle + xMargin2);
	}

	Line* getLineUnderMouse(int x, int y) {
		Line[] lines;
		y -= top + yMargin;
		if (y < 0) return null;
		if (x >= left + xMargin0 && x < middle - xMargin1) {
			lines = leftLines;
		} else if (x >= middle + xMargin2 && x < right - xMargin3) {
			lines = rightLines;
		} else {
			return null;
		}
		y /= fontHeight;
		if (y < lines.length && lines[y].medication != Medication.None) {
			return &lines[y];
		}
		return null;
	}

	void onMouseDown(ref ALLEGRO_EVENT event) {
		assert(!isFinishing());

		if (event.mouse.x < left || event.mouse.x >= right || event.mouse.y < top || event.mouse.y >= bottom) {
			startHiding();
			return;
		}

		auto line = getLineUnderMouse(event.mouse.x, event.mouse.y);
		if (line) {
			if (patient.prescription == line.medication) {
				patient.cancelPrescription();
			} else {
				patient.startPrescription(line.medication);
				theTutorial.onPrescriptionWritten();
			}
			build();
		}
	}
}
