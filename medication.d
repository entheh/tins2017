import std.random;

enum Medication {
	None,
	Antipyretic,
	Propyretic,
	Laxative,
	Binder,
	Antiblobotics,
	Problobotics,
}

immutable string[] allergyTriggerStrings = [
	"No allergies",
	"Allergy to antipyretics:",
	"Allergy to propyretics:",
	"Allergy to laxatives:",
	"Allergy to binders:",
	"Allergy to antiblobotics:",
	"Allergy to problobotics:",
];

immutable string[] allergyConsequenceStrings = [
	null,
	"causes chills",
	"causes fever",
	"causes diarrhoea",
	"causes constipation",
	null,
	"causes infection",
];

immutable string[] prescriptionStrings = [
	null,
	"antipyretic",
	"propyretic",
	"laxative",
	"binder",
	"antiblobotics",
	"problobotics",
];

Medication opposite(Medication m) {
	assert(m != Medication.None);
	return cast(Medication)(((m - 1) ^ 1) + 1);
}

Medication randomMedication() {
	return uniform!"[]"(Medication.Antipyretic, Medication.Problobotics);
}

Medication randomAilment() {
	Medication m = uniform!"[]"(Medication.Antipyretic, cast(Medication)(Medication.Problobotics - 1));
	if (m == Medication.Antiblobotics) m++;
	return m;
}

Medication randomAllergyConsequence(Medication allergyTrigger) {
	if (allergyTrigger >= Medication.Antiblobotics) {
		return uniform!"[]"(Medication.Antipyretic, Medication.Binder);
	} else {
		//Don't choose Antiblobotics (cures infection) as an allergy consequence,
		//since there's no negative infection state.
		Medication m = uniform!"[]"(Medication.Antipyretic, cast(Medication)(Medication.Problobotics - 3));
		if (m >= ((allergyTrigger - 1) & ~1) + 1) {
			m += 2;
		}
		if (m == Medication.Antiblobotics) m++;
		return m;
	}
}
