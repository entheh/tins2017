import std.algorithm.comparison;

enum FPS = 100;
enum approxMaxUpdatesBetweenDraws = 20;

enum prescriptionLength = 14;
enum prescriptionInterval = 1*FPS; //Note: must match tutorial text

enum maxSeverity = 28;
enum minPresentingSeverity = 7;
enum maxPresentingSeverity = 14;

enum patientRadius = 20;
enum patientHeight = 92;
enum minX = 35, maxX = 35+737;
enum minY = 70/*53*/, maxY = 53+543;  //53 corresponds to the image, but 70 ensures we can mostly see patients' heads
enum minXExiting = 324, maxXExiting = 324+167;
enum maxYExiting = 600 + patientHeight + 40;

enum patientSpeedAgitated = 1;
enum patientSpeedPrescribed = 0.5;
enum patientAccel = 0.02;

enum InteractionDistances {
	patientAvoidance = 40,
	patientExplosion = 40,
	patientDiarrhoea = 30,
	diarrhoeaFireTransmission = 25,
}

enum patientNewTargetThreshold = 50;

enum walkMorphFrames = 32;
enum danceMorphFrames = 32;
enum sneezeMorphFrames = 16;
enum blobbinessMorphFrames = 32;
enum blobWalkMorphFrames = 32;
enum diarrhoeaToFireMorphFrames = 64;
enum fireMorphFrames = 16;

enum sneezeLength = sneezeMorphFrames * 3;
enum minSneezeInterval = sneezeLength + 10;
enum maxSneezeInterval = 10*FPS;

enum diarrhoeaInterval = 20;
enum explosionLifetime = 40;
enum explosionTransmitDelay = 10;
enum diarrhoeaFireLifetime = 2*FPS;
enum diarrhoeaPatientBurnDelay = 50;
enum diarrhoeaFireShrinkLength = FPS/2;
enum patientFireLifetime = 3*FPS;
enum diarrhoeaInteractionInterval = FPS/4;
enum diarrhoeaFireTransmissionTime = FPS*3/2;
enum maxDiarrhoeas = 1000;
enum diarrhoeaRadius = 10;

enum contagionInteractionInterval = sneezeLength + 1;

enum gravity = 0.1;

enum diarrhoeaSpeedXy = 4;
enum diarrhoeaSpeedZ = 2;
enum numDiarrhoeaFromExplosion = 30;

enum numStartingPatients = 5;

enum pointsForCuring = 1;
enum pointsForKilling = 10;
