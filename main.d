import std.stdio;
import std.exception;
import std.algorithm.sorting;
import std.algorithm.mutation;
import std.math;
import std.typecons;
import allegro5.allegro;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_audio;
import allegro5.allegro_acodec;
import constants;
import game;
import morpher;
import drawable;
import tutorial;
import title;

int main(string[] args) {
	debug {
		try {
			return main2(args);
		} catch (Throwable t) {
			writeln(t.toString());
			writeln("Press Enter to close...");
			readln();
			return 1;
		}
	} else {
		return main2(args);
	}
}

struct Gfx {
	ALLEGRO_BITMAP*[2] blobs;
	ALLEGRO_BITMAP* chills;
	ALLEGRO_BITMAP* constipated;
	ALLEGRO_BITMAP*[3] dances;
	ALLEGRO_BITMAP* diarrhoea;
	ALLEGRO_BITMAP* explosion;
	ALLEGRO_BITMAP* fever;
	ALLEGRO_BITMAP*[2] fires;
	ALLEGRO_BITMAP* front;
	ALLEGRO_BITMAP* healthy;
	ALLEGRO_BITMAP* incontinent;
	ALLEGRO_BITMAP* infected;
	ALLEGRO_BITMAP*[2] prescriptions;
	ALLEGRO_BITMAP* selection;
	ALLEGRO_BITMAP* sneeze;
	ALLEGRO_BITMAP* surgery;
	ALLEGRO_BITMAP* unhealthy;
	ALLEGRO_BITMAP*[2] walks;
}
Gfx gfx;

ALLEGRO_FONT* theFont;
int fontHeight;

struct Sfx {
	ALLEGRO_SAMPLE* burst;
	ALLEGRO_SAMPLE*[11] diarrhoeas;
	ALLEGRO_SAMPLE* fire;
	ALLEGRO_SAMPLE* sneeze;
	ALLEGRO_SAMPLE_INSTANCE* fireInstance;
	float fireLeftGain, fireRightGain;
}
Sfx sfx;

class Morphers {
	Unique!Morpher walk;
	Unique!Morpher[2] dances;
	Unique!Morpher fever;
	Unique!Morpher chills;
	Unique!Morpher constipated;
	Unique!Morpher diarrhoea;
	Unique!Morpher infected;
	Unique!Morpher sneeze;
	Unique!Morpher blobbiness;
	Unique!Morpher blobWalk;
	Unique!Morpher diarrhoeaToFire;
	Unique!Morpher fire;
}
Morphers morphers;

int globalTicks;
int titleState;

int main2(string[] argv) {
	return al_run_allegro({
		enforce(al_init());
		enforce(al_init_image_addon());
		enforce(al_init_font_addon());
		enforce(al_init_ttf_addon());
		enforce(al_install_audio());
		enforce(al_reserve_samples(64));
		enforce(al_init_acodec_addon());

		auto eventQueue = enforce(al_create_event_queue());

		enforce(al_install_keyboard());
		al_register_event_source(eventQueue, al_get_keyboard_event_source());

		enforce(al_install_mouse());
		al_register_event_source(eventQueue, al_get_mouse_event_source());

		al_set_new_window_title("Dr Havoc Shitfan");
		al_set_new_display_flags(ALLEGRO_WINDOWED);
		auto display = enforce(al_create_display(800, 600));
		al_register_event_source(eventQueue, al_get_display_event_source(display));

		gfx.blobs[0] = enforce(al_load_bitmap("gfx/blob0.png"));
		gfx.blobs[1] = enforce(al_load_bitmap("gfx/blob1.png"));
		gfx.chills = enforce(al_load_bitmap("gfx/chills.png"));
		gfx.constipated = enforce(al_load_bitmap("gfx/constipated.png"));
		gfx.dances[0] = enforce(al_load_bitmap("gfx/dance0.png"));
		gfx.dances[1] = enforce(al_load_bitmap("gfx/dance1.png"));
		gfx.dances[2] = enforce(al_load_bitmap("gfx/dance2.png"));
		gfx.diarrhoea = enforce(al_load_bitmap("gfx/diarrhoea.png"));
		gfx.explosion = enforce(al_load_bitmap("gfx/explosion.png"));
		gfx.fever = enforce(al_load_bitmap("gfx/fever.png"));
		gfx.fires[0] = enforce(al_load_bitmap("gfx/fire0.png"));
		gfx.fires[1] = enforce(al_load_bitmap("gfx/fire1.png"));
		gfx.front = enforce(al_load_bitmap("gfx/front.png"));
		gfx.healthy = enforce(al_load_bitmap("gfx/healthy.png"));
		gfx.incontinent = enforce(al_load_bitmap("gfx/incontinent.png"));
		gfx.infected = enforce(al_load_bitmap("gfx/infected.png"));
		gfx.prescriptions[0] = enforce(al_load_bitmap("gfx/prescription0.png"));
		gfx.prescriptions[1] = enforce(al_load_bitmap("gfx/prescription1.png"));
		gfx.selection = enforce(al_load_bitmap("gfx/selection.png"));
		gfx.sneeze = enforce(al_load_bitmap("gfx/sneeze.png"));
		gfx.surgery = enforce(al_load_bitmap("gfx/surgery.png"));
		gfx.unhealthy = enforce(al_load_bitmap("gfx/unhealthy.png"));
		gfx.walks[0] = enforce(al_load_bitmap("gfx/walk0.png"));
		gfx.walks[1] = enforce(al_load_bitmap("gfx/walk1.png"));

		al_set_display_icon(display, gfx.healthy);

		theFont = enforce(al_load_ttf_font("font/Brushaff.otf", 16, 0));
		fontHeight = al_get_font_line_height(theFont);

		auto music = enforce(al_load_audio_stream("audio/DrHavocShitfan.it", 2, 4096));
		scope (exit) al_destroy_audio_stream(music);
		enforce(al_set_audio_stream_playmode(music, ALLEGRO_PLAYMODE.ALLEGRO_PLAYMODE_LOOP));
		enforce(al_attach_audio_stream_to_mixer(music, al_get_default_mixer()));

		sfx.burst = enforce(al_load_sample("audio/burst.wav"));
		sfx.diarrhoeas[0] = enforce(al_load_sample("audio/diarrhoea0.wav"));
		sfx.diarrhoeas[1] = enforce(al_load_sample("audio/diarrhoea1.wav"));
		sfx.diarrhoeas[2] = enforce(al_load_sample("audio/diarrhoea2.wav"));
		sfx.diarrhoeas[3] = enforce(al_load_sample("audio/diarrhoea3.wav"));
		sfx.diarrhoeas[4] = enforce(al_load_sample("audio/diarrhoea4.wav"));
		sfx.diarrhoeas[5] = enforce(al_load_sample("audio/diarrhoea5.wav"));
		sfx.diarrhoeas[6] = enforce(al_load_sample("audio/diarrhoea6.wav"));
		sfx.diarrhoeas[7] = enforce(al_load_sample("audio/diarrhoea7.wav"));
		sfx.diarrhoeas[8] = enforce(al_load_sample("audio/diarrhoea8.wav"));
		sfx.diarrhoeas[9] = enforce(al_load_sample("audio/diarrhoea9.wav"));
		sfx.diarrhoeas[10] = enforce(al_load_sample("audio/diarrhoea10.wav"));
		sfx.fire = enforce(al_load_sample("audio/fire.wav"));
		sfx.sneeze = enforce(al_load_sample("audio/sneeze.wav"));

		sfx.fireInstance = enforce(al_create_sample_instance(sfx.fire));
		enforce(al_set_sample_instance_playmode(sfx.fireInstance, ALLEGRO_PLAYMODE.ALLEGRO_PLAYMODE_LOOP));
		enforce(al_set_sample_instance_gain(sfx.fireInstance, 0));
		enforce(al_set_sample_instance_playing(sfx.fireInstance, true));
		enforce(al_attach_sample_instance_to_mixer(sfx.fireInstance, al_get_default_mixer()));

		//The morphers take some time to generate. Display a title screen with morph progress as we go along.
		morphers = new Morphers();
		scope (exit) delete morphers;
		int progress = 0;
		enum maxProgress = 13;
		drawTitleScreen(progress++, maxProgress); morphers.walk = Unique!Morpher(new Morpher(gfx.walks[0], gfx.walks[1], walkMorphFrames));
		drawTitleScreen(progress++, maxProgress); morphers.dances[0] = Unique!Morpher(new Morpher(gfx.dances[0], gfx.dances[1], danceMorphFrames));
		drawTitleScreen(progress++, maxProgress); morphers.dances[1] = Unique!Morpher(new Morpher(gfx.dances[0], gfx.dances[2], danceMorphFrames));
		drawTitleScreen(progress++, maxProgress); morphers.fever = Unique!Morpher(new Morpher(gfx.unhealthy, gfx.fever, maxSeverity));
		drawTitleScreen(progress++, maxProgress); morphers.chills = Unique!Morpher(new Morpher(gfx.unhealthy, gfx.chills, maxSeverity));
		drawTitleScreen(progress++, maxProgress); morphers.constipated = Unique!Morpher(new Morpher(gfx.unhealthy, gfx.constipated, maxSeverity));
		drawTitleScreen(progress++, maxProgress); morphers.diarrhoea = Unique!Morpher(new Morpher(gfx.unhealthy, gfx.incontinent, maxSeverity));
		drawTitleScreen(progress++, maxProgress); morphers.infected = Unique!Morpher(new Morpher(gfx.unhealthy, gfx.infected, maxSeverity));
		drawTitleScreen(progress++, maxProgress); morphers.sneeze = Unique!Morpher(new Morpher(gfx.infected, gfx.sneeze, sneezeMorphFrames));
		drawTitleScreen(progress++, maxProgress); morphers.blobbiness = Unique!Morpher(new Morpher(gfx.walks[0], gfx.blobs[0], blobbinessMorphFrames));
		drawTitleScreen(progress++, maxProgress); morphers.blobWalk = Unique!Morpher(new Morpher(gfx.blobs[0], gfx.blobs[1], blobWalkMorphFrames));
		drawTitleScreen(progress++, maxProgress); morphers.diarrhoeaToFire = Unique!Morpher(new Morpher(gfx.diarrhoea, gfx.fires[0], diarrhoeaToFireMorphFrames));
		drawTitleScreen(progress++, maxProgress); morphers.fire = Unique!Morpher(new Morpher(gfx.fires[0], gfx.fires[1], fireMorphFrames));
		assert(progress == maxProgress);
		titleState = 2;

		theGame = new Game();
		theTutorial = new Tutorial();

		auto timer = enforce(al_create_timer(1.0 / FPS));
		al_register_event_source(eventQueue, al_get_timer_event_source(timer));
		al_start_timer(timer);

		int updatesToProcess = 0;
		int updatesSinceDraw = 0;
		void processUpdateBatch() {
			if (updatesToProcess > 0) {
				update(updatesToProcess);
				updatesSinceDraw += updatesToProcess;
				globalTicks += updatesToProcess;
				updatesToProcess = 0;
			}
		}
	mainLoop:
		while (true) {
			ALLEGRO_EVENT event;
			if (al_is_event_queue_empty(eventQueue)) {
				if (titleState == 2) titleState = 1;
				//Keep up to date on updates if the queue is empty (including timer events)
				processUpdateBatch();
				//If queue still empty, draw a frame (if there is an update)
				if (al_is_event_queue_empty(eventQueue) && updatesSinceDraw > 0) {
					draw();
					updatesSinceDraw = 0;
				}
			}
			//Must draw sometimes even if updates are taking ages
			if (updatesSinceDraw >= approxMaxUpdatesBetweenDraws) {
				draw();
				updatesSinceDraw = 0;
			}
			//Get the next event (possibly waiting)
			al_wait_for_event(eventQueue, &event);
			if (event.type == ALLEGRO_EVENT_TIMER) {
				updatesToProcess++;
			} else {
				//Non-timer event - make sure updates are up to date before processing it
				processUpdateBatch();
				switch (event.type) {
					case ALLEGRO_EVENT_DISPLAY_CLOSE:
						break mainLoop;
					case ALLEGRO_EVENT_KEY_DOWN:
						if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
							if (theGame.patientPopup) {
								theGame.patientPopup = null;
							} else {
								break mainLoop;
							}
						}
						break;
					case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
						if (titleState) {
							if (titleState == 1) titleState = 0;
						} else if (theTutorial.noPatientsLeft()) {
							theTutorial = new Tutorial();
							theGame = new Game();
						} else {
							theGame.onMouseDown(event);
						}
						break;
					default:
						break;
				}
			}
		}
		return 0;
	});
}

void update(int numTicks) {
	if (titleState) return;

	theGame.update(numTicks);
}

void draw() {
	if (titleState) {
		drawTitleScreen(1, 1);
		return;
	}

	sfx.fireLeftGain = sfx.fireRightGain = 0;

	ALLEGRO_MOUSE_STATE mouse;
	al_get_mouse_state(&mouse);

	al_draw_bitmap(gfx.surgery, 0, 0, 0);
	theTutorial.draw();
	theGame.populateReusableDrawables();
	sort!("a.y < b.y", SwapStrategy.stable)(theGame.reusableDrawables);
	foreach (drawable; theGame.reusableDrawables) {
		drawable.draw();
	}
	if (!theGame.patientPopup || theGame.patientPopup.isFinishing()) {
		if (mouse.display) {
			auto patient = theGame.getPatientUnderMouse(mouse.x, mouse.y);
			if (patient) {
				int w = al_get_bitmap_width(gfx.selection);
				int h = al_get_bitmap_height(gfx.selection);
				al_draw_bitmap(gfx.selection, patient.x-w/2, patient.y-(patientHeight+h)/2, 0);
			}
		}
	}
	if (theGame.patientPopup && !theGame.patientPopup.isFinishing()) {
		auto patient = theGame.patientPopup.patient;
		int w = al_get_bitmap_width(gfx.selection);
		int h = al_get_bitmap_height(gfx.selection);
		al_draw_bitmap(gfx.selection, patient.x-w/2, patient.y-(patientHeight+h)/2, 0);
	}
	al_draw_bitmap(gfx.front, 0, 600 - al_get_bitmap_height(gfx.front), 0);
	al_draw_textf(theFont, al_map_rgb(248,238,216), 10, 570, ALLEGRO_ALIGN_LEFT, "Score: %d", theGame.score);
	al_draw_textf(theFont, al_map_rgb(248,238,216), 800-10, 570, ALLEGRO_ALIGN_RIGHT, "Time left: %d:%02d", (theGame.timeLeft+FPS-1) / (60*FPS), ((theGame.timeLeft+FPS-1) / FPS) % 60);
	if (theGame.patientPopup) {
		theGame.patientPopup.draw(mouse);
	}
	al_flip_display();

	float gain = (sfx.fireLeftGain + sfx.fireRightGain) * 0.5;
	float pan = (gain < 0.0001 ? 0 : (sfx.fireRightGain / gain) - 1);
	if (gain > 0.4) gain = 0.4;
	enforce(al_set_sample_instance_gain(sfx.fireInstance, sqrt(gain) * 0.63));
	enforce(al_set_sample_instance_pan(sfx.fireInstance, pan));
}
