import allegro5.allegro;
import std.random;
import std.math;

//We use the Lucas-Kanade method, sort of.
//https://en.wikipedia.org/wiki/Lucas%E2%80%93Kanade_method
class Morpher {
	ALLEGRO_BITMAP*[] frames;

	this(ALLEGRO_BITMAP* a, ALLEGRO_BITMAP* b, int count) {
		frames.length = count + 1;
		frames[0] = a;
		frames[count] = b;

		auto al = al_lock_bitmap(a, ALLEGRO_PIXEL_FORMAT.ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA, ALLEGRO_LOCK_READONLY);
		scope (exit) al_unlock_bitmap(a);
		auto bl = al_lock_bitmap(b, ALLEGRO_PIXEL_FORMAT.ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA, ALLEGRO_LOCK_READONLY);
		scope (exit) al_unlock_bitmap(b);

		enum channels = 4;

		//Create a few smaller versions of the images.
		struct Layer {
			int w, h;
			ubyte* ptr;
			int stride;
			ubyte[] alloc;

			this(ALLEGRO_BITMAP* image, ALLEGRO_LOCKED_REGION* lock) {
				w = al_get_bitmap_width(image);
				h = al_get_bitmap_height(image);
				ptr = cast(ubyte*)lock.data;
				stride = lock.pitch;
			}

			this(Layer parent) {
				w = parent.w/2;
				h = parent.h/2;
				alloc.length = w*h*channels;
				ptr = alloc.ptr;
				stride = w*channels;
				ubyte* pixel = ptr;
				foreach (y; 0..h) {
					foreach (x; 0..w) {
						auto parentPixel = parent.ptr + y*2*parent.stride + x*8;
						foreach (c; 0..channels) {
							*pixel++ =
								(parentPixel[0] +
								 parentPixel[channels] +
								 parentPixel[parent.stride] +
								 parentPixel[parent.stride+channels] + 2) >> 2;
							parentPixel++;
						}
					}
				}
			}
		}
		enum numLayers = 4;
		Layer[] aLayers;
		Layer[] bLayers;
		aLayers ~= Layer(a, al);
		bLayers ~= Layer(b, bl);
		foreach (i; 1..numLayers) {
			aLayers ~= Layer(aLayers[i-1]);
			bLayers ~= Layer(bLayers[i-1]);
		}

		//Compute a position offset for each point in a grid.
		//Positive offsets mean a pixel towards the negative direction in 'a' matches one towards a positive direction in 'b'.
		enum gw = 16, gh = 16;
		struct Offset {
			float x = 0, y = 0;
			float score = -1;
			float xProposed = 0, yProposed = 0;
		}
		Offset[gw][gh] offsets;

		foreach_reverse (li; 0..numLayers) {
			auto aLayer = &aLayers[li];
			auto bLayer = &bLayers[li];

			int aw = aLayer.w;
			int ah = aLayer.h;
			int bw = bLayer.w;
			int bh = bLayer.h;

			//grid square sizes in each image
			float agw = (aw-1)/(gw-1.0);
			float agh = (ah-1)/(gh-1.0);
			float bgw = (bw-1)/(gw-1.0);
			float bgh = (bh-1)/(gh-1.0);

			enum numIterations = 5;
			foreach (iteration; 0..numIterations+1) {
				bool isFinishing = (iteration == numIterations);

				foreach (gy; 0..gh) {
					foreach (gx; 0..gw) {
						auto offset = &offsets[gy][gx];

						//Centre coordinates in each image
						float acx = gx*(aw-1)/(gw-1.0);
						float acy = gy*(ah-1)/(gh-1.0);
						float bcx = gx*(bw-1)/(gw-1.0);
						float bcy = gy*(bh-1)/(gh-1.0);

						//Take a number of pixel samples from each image
						enum numSamples = 64;
						struct Sample {
							float value;
							float xgrad, ygrad; //brightness change per grid square
						}
						Sample[channels][numSamples][2] samples;
						foreach (s; 0..numSamples) {
							//Box-Muller algorithm for choosing two Gaussian random numbers
							//http://www.design.caltech.edu/erik/Misc/Gaussian.html
							/*
							float sx, sy, w;
							float stddev = 1.0 * (1 << li);
							do {
								sx = uniform!"()"(-1.0, 1.0);
								sy = uniform!"()"(-1.0, 1.0);
								w = sx*sx + sy*sy;
							} while (w >= 1);
							w = sqrt((-2*log(w))/w) * stddev;
							sx *= w;
							sy *= w;
							*/
							float sx = ((s&7)  - 3.5) * (2 / 3.5);// * (1 << li);
							float sy = ((s>>3) - 3.5) * (2 / 3.5);// * (1 << li);
							//Sample coordinates in images - adjusted by the offset so far
							float asx = acx + (sx - offset.xProposed) * agw;
							float asy = acy + (sy - offset.yProposed) * agh;
							float bsx = bcx + (sx + offset.xProposed) * bgw;
							float bsy = bcy + (sy + offset.yProposed) * bgh;
							//Bilinear sampling helper function
							void getSample(ref Layer image, float igw, float igh, float xf, float yf, ref Sample[channels] sample) {
								int xi = cast(int)floor(xf);
								int yi = cast(int)floor(yf);
								xf -= xi;
								yf -= yi;
								auto ptr = image.ptr + yi*image.stride + xi*channels;
								ubyte[channels] p01=0, p02=0, p10=0, p11=0, p12=0, p13=0, p20=0, p21=0, p22=0, p23=0, p31=0, p32=0;
								ptr -= image.stride; yi--;
								if (yi>=0 && yi<image.h) {
									if (xi>=0 && xi<image.w) p01 = *cast(ubyte[channels]*)ptr;
									if (xi+1>=0 && xi+1<image.w) p02 = *cast(ubyte[channels]*)(ptr + channels);
								}
								ptr += image.stride; yi++;
								if (yi>=0 && yi<image.h) {
									if (xi-1>=0 && xi-1<image.w) p10 = *cast(ubyte[channels]*)(ptr - channels);
									if (xi>=0 && xi<image.w) p11 = *cast(ubyte[channels]*)ptr;
									if (xi+1>=0 && xi+1<image.w) p12 = *cast(ubyte[channels]*)(ptr + channels);
									if (xi+2>=0 && xi+2<image.w) p13 = *cast(ubyte[channels]*)(ptr + channels*2);
								}
								ptr += image.stride; yi++;
								if (yi>=0 && yi<image.h) {
									if (xi-1>=0 && xi-1<image.w) p20 = *cast(ubyte[channels]*)(ptr - channels);
									if (xi>=0 && xi<image.w) p21 = *cast(ubyte[channels]*)ptr;
									if (xi+1>=0 && xi+1<image.w) p22 = *cast(ubyte[channels]*)(ptr + channels);
									if (xi+2>=0 && xi+2<image.w) p23 = *cast(ubyte[channels]*)(ptr + channels*2);
								}
								ptr += image.stride; yi++;
								if (yi>=0 && yi<image.h) {
									if (xi>=0 && xi<image.w) p31 = *cast(ubyte[channels]*)ptr;
									if (xi+1>=0 && xi+1<image.w) p32 = *cast(ubyte[channels]*)(ptr + channels);
								}
								foreach (c; 0..channels) {
									float interp(float p00, float p01, float p10, float p11) {
										float p0 = p00 + (p01 - p00) * xf;
										float p1 = p10 + (p11 - p10) * xf;
										return p0 + (p1 - p0) * yf;
									}
									sample[c].value = interp(p11[c], p12[c], p21[c], p22[c]);
									sample[c].xgrad = 0.5f * interp(p12[c]-p10[c], p13[c]-p11[c], p22[c]-p20[c], p23[c]-p21[c]) * igw;
									sample[c].ygrad = 0.5f * interp(p21[c]-p01[c], p22[c]-p02[c], p31[c]-p11[c], p32[c]-p12[c]) * igh;
								}
							}
							getSample(*aLayer, agw, agh, asx, asy, samples[0][s]);
							getSample(*bLayer, bgw, bgh, bsx, bsy, samples[1][s]);
						}

						//Normalise each sample set
						void normalise(ref Sample[channels][numSamples] samples) {
							//Compute the average
							float[channels] avg = 0;
							foreach (ref sample; samples) {
								foreach (c; 0..channels) {
									avg[c] += sample[c].value;
								}
							}
							//Subtract the average off
							avg[] /= numSamples;
							foreach (ref sample; samples) {
								foreach (c; 0..channels) {
									sample[c].value -= avg[c];
								}
							}
							//Compute the variance
							float var = 0;
							foreach (ref sample; samples) {
								foreach (c; 0..channels) {
									var += sample[c].value * sample[c].value;
								}
							}
							var /= numSamples * channels;
							//Normalise the variance
							if (var > 0.01) {
								var = 1/sqrt(var);
								foreach (ref sample; samples) {
									foreach (c; 0..channels) {
										sample[c].value *= var;
										sample[c].xgrad *= var;
										sample[c].ygrad *= var;
									}
								}
							}
						}
						normalise(samples[0]);
						normalise(samples[1]);

						//Compute a similarity score for the two sample sets.
						float score = 0;
						foreach (s; 0..numSamples) {
							foreach (c; 0..channels) {
								score += samples[0][s][c].value * samples[1][s][c].value;
							}
						}
						score /= numSamples * channels;

						//If the score has improved, we can take the proposed improvement from last iteration.
						if (score > offset.score) {
							offset.x = offset.xProposed;
							offset.y = offset.yProposed;
							offset.score = score;
						} else {
							//Otherwise throw it away and come back next iteration.
							offset.xProposed = offset.x;
							offset.yProposed = offset.y;
							continue;
						}

						//Out of iterations? Don't bother calculating another 'proposed'.
						if (isFinishing) {
							continue;
						}

						//Look at differences and gradients to see how we should shift the images.
						//Here we have the matrix maths from https://en.wikipedia.org/wiki/Lucas%E2%80%93Kanade_method
						float xgrads = 0;
						float crossgrads = 0;
						float ygrads = 0;
						float xvalues = 0;
						float yvalues = 0;
						foreach (s; 0..numSamples) {
							foreach (c; 0..channels) {
								float diff = samples[1][s][c].value - samples[0][s][c].value;
								float xgrad = 0.5 * (samples[0][s][c].xgrad + samples[0][s][c].xgrad);
								float ygrad = 0.5 * (samples[0][s][c].ygrad + samples[0][s][c].ygrad);
								xgrads += xgrad * xgrad;
								crossgrads += xgrad * ygrad;
								ygrads += ygrad * ygrad;
								xvalues -= xgrad * diff;
								yvalues -= ygrad * diff;
							}
						}
						//Now invert the A matrix
						//[xgrads crossgrads]^-1 = [ygrads -crossgrads] * 1/det
						//[crossgrads ygrads]      [-crossgrads xgrads]
						//where det = ad-bc = xgrads*ygrads - crossgrads^2
						float det = xgrads*ygrads - crossgrads*crossgrads;
						//And compute xflow,yflow = A^-1 * [xvalues, yvalues]^T
						float xflow = ygrads*xvalues - crossgrads*yvalues;
						float yflow = xgrads*yvalues - crossgrads*xvalues;
						//Numerical stability sanity check
						float mag = sqrt(xflow*xflow + yflow*yflow);
						if (mag >= 0.01) {
							//Don't 'flow' too much per iteration
							enum maxPixelsPerIteration = 10;
							if (det < mag/maxPixelsPerIteration) {
								det = mag/maxPixelsPerIteration;
							}
							//The offset gets applied to both image read positions, so halve it
							offset.xProposed = offset.x + 0.5*xflow/det;
							offset.yProposed = offset.y + 0.5*yflow/det;
						}
					}
				}

				//After each non-final iteration, allow each offset to influence the others.
				//Helps keep outliers under control and make everything move together.
				if (!isFinishing) {
					Offset[gw][gh] offsetsCopy = offsets;
					foreach (gy; 0..gh) {
						foreach (gx; 0..gw) {
							//Weight this grid square (versus its neighbours) more on finer pyramid layers.
							//i.e. trust individual results more the finer we get.
							int num = 1 << (numLayers - 1 - li);
							offsets[gy][gx].xProposed *= num;
							offsets[gy][gx].yProposed *= num;
							if (gx > 0) {
								offsets[gy][gx].xProposed += offsets[gy][gx-1].xProposed;
								offsets[gy][gx].yProposed += offsets[gy][gx-1].yProposed;
								num++;
							}
							if (gx < gw-1) {
								offsets[gy][gx].xProposed += offsets[gy][gx+1].xProposed;
								offsets[gy][gx].yProposed += offsets[gy][gx+1].yProposed;
								num++;
							}
							if (gy > 0) {
								offsets[gy][gx].xProposed += offsets[gy-1][gx].xProposed;
								offsets[gy][gx].yProposed += offsets[gy-1][gx].yProposed;
								num++;
							}
							if (gy < gh-1) {
								offsets[gy][gx].xProposed += offsets[gy+1][gx].xProposed;
								offsets[gy][gx].yProposed += offsets[gy+1][gx].yProposed;
								num++;
							}
							offsets[gy][gx].xProposed /= num;
							offsets[gy][gx].yProposed /= num;
						}
					}
				}
			}
		}

		//Offsets calculated - now we can generate the intermediate bitmaps.
		int aw = al_get_bitmap_width(a);
		int ah = al_get_bitmap_height(a);
		int bw = al_get_bitmap_width(b);
		int bh = al_get_bitmap_height(b);
		float agw = (aw-1)/(gw-1.0);
		float agh = (ah-1)/(gh-1.0);
		float bgw = (bw-1)/(gw-1.0);
		float bgh = (bh-1)/(gh-1.0);
		foreach (i, ref frame; frames[1..$-1]) {
			float t = i / cast(float)count;

			int w = aw + cast(int)round((bw - aw) * t);
			int h = ah + cast(int)round((bh - ah) * t);

			frame = al_create_bitmap(w, h);

			auto lock = al_lock_bitmap(frame, ALLEGRO_PIXEL_FORMAT.ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA, ALLEGRO_LOCK_WRITEONLY);
			scope (exit) al_unlock_bitmap(frame);

			auto row = lock.data;
			foreach (y; 0..h) {
				auto dest = row;
				foreach (x; 0..w) {
					//Interpolate the offsets from the grid
					float gxf = x*(gw-1)/(w-1.0);
					float gyf = y*(gh-1)/(h-1.0);
					int gxi = cast(int)floor(gxf);
					int gyi = cast(int)floor(gyf);
					if (gxi>=gw-1) gxi--;
					if (gyi>=gh-1) gyi--;
					gxf -= gxi;
					gyf -= gyi;
					auto o00 = &offsets[gyi][gxi];
					auto o01 = &offsets[gyi][gxi+1];
					auto o10 = &offsets[gyi+1][gxi];
					auto o11 = &offsets[gyi+1][gxi+1];
					auto o0x = o00.x + (o01.x - o00.x) * gxf;
					auto o1x = o10.x + (o11.x - o10.x) * gxf;
					auto ox = o0x + (o1x - o0x) * gyf;
					auto o0y = o00.y + (o01.y - o00.y) * gxf;
					auto o1y = o10.y + (o11.y - o10.y) * gxf;
					auto oy = o0y + (o1y - o0y) * gyf;

					//Here, we aren't applying offsets simultaneously to both images, just one at a time (or part and part),
					//so we must double them.
					ox *= 2;
					oy *= 2;

					//Choose positions to sample from in the source and destination images
					float axf = x * aw / cast(float)w - ox*agw*t;
					float ayf = y * ah / cast(float)h - oy*agh*t;
					float bxf = x * bw / cast(float)w + ox*bgw*(1-t);
					float byf = y * bh / cast(float)h + oy*bgh*(1-t);

					//Bilinear sampling helper function
					void getPixel(ALLEGRO_LOCKED_REGION* image, int iw, int ih, float xf, float yf, ref float[channels] pixel) {
						int xi = cast(int)floor(xf);
						int yi = cast(int)floor(yf);
						xf -= xi;
						yf -= yi;
						auto ptr = image.data + yi*image.pitch + xi*channels;
						ubyte[channels] p00=0, p01=0, p10=0, p11=0;
						if (yi>=0 && yi<ih) {
							if (xi>=0 && xi<iw) p00 = *cast(ubyte[channels]*)ptr;
							if (xi+1>=0 && xi+1<iw) p01 = *cast(ubyte[channels]*)(ptr + channels);
						}
						ptr += image.pitch; yi++;
						if (yi>=0 && yi<ih) {
							if (xi>=0 && xi<iw) p10 = *cast(ubyte[channels]*)ptr;
							if (xi+1>=0 && xi+1<iw) p11 = *cast(ubyte[channels]*)(ptr + channels);
						}
						foreach (c; 0..channels) {
							float p0 = p00[c] + (p01[c] - p00[c]) * xf;
							float p1 = p10[c] + (p11[c] - p10[c]) * xf;
							pixel[c] = p0 + (p1 - p0) * yf;
						}
					}
					float[channels] aPixel;
					float[channels] bPixel;
					getPixel(al, aw, ah, axf, ayf, aPixel);
					getPixel(bl, bw, bh, bxf, byf, bPixel);

					//Compute the destination pixel
					float[channels] result = aPixel[] + (bPixel[] - aPixel[]) * t;
					foreach (c; 0..channels) {
						(cast(ubyte*)dest)[c] = cast(ubyte)round(result[c]);
					}
					dest += lock.pixel_size;
				}
				row += lock.pitch;
			}
		}
	}

	~this() {
		foreach (frame; frames[1..$-1]) {
			al_destroy_bitmap(frame);
		}
	}
}
