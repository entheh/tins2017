import drawable;
import allegro5.allegro;
import main;
import constants;
import diarrhoea;
import std.random;
import game;

class Explosion : Drawable {
	int lifetime = explosionLifetime;
	bool isPrimary;

	void createDiarrhoea() {
		foreach (i; 0..numDiarrhoeaFromExplosion) {
			auto diarrhoea = new Diarrhoea();
			diarrhoea.x = x;
			diarrhoea.y = y;
			diarrhoea.z = uniform!"[]"(0.25, 0.75) * patientHeight;
			with (diarrhoea) {
				do {
					dx = uniform!"()"(-1.0, 1.0);
					dy = uniform!"()"(-1.0, 1.0);
					dz = uniform!"()"(-1.0, 1.0);
				} while (dx*dx + dy*dy + dz*dz > 1);
			}
			diarrhoea.dx *= diarrhoeaSpeedXy;
			diarrhoea.dy *= diarrhoeaSpeedXy;
			diarrhoea.dz *= diarrhoeaSpeedZ;
			theGame.diarrhoeas ~= diarrhoea;
		}
	}

	bool update(int numTicks) {
		lifetime -= numTicks;
		if (lifetime <= 0) {
			return false;
		}
		return true;
	}

	override void draw() {
		float size = lifetime * (explosionLifetime - lifetime) / (explosionLifetime * explosionLifetime * 0.25);
		int w = al_get_bitmap_width(gfx.explosion);
		int h = al_get_bitmap_height(gfx.explosion);
		al_draw_scaled_bitmap(gfx.explosion, 0, 0, w, h, x - w*size*0.5, y - h*(0.5+size*0.5), w*size, h*size, 0);
	}
}
