import constants;
import std.random;
import drawable;
import patient;
import std.math;
import explosion;
import game;
import diarrhoea;
import std.algorithm.comparison;
import main;

struct Interaction {
	int numTicks;
	float dx, dy;
	float dist;

	void interact(Drawable p, Drawable n) {
		if (!interact2(p, n)) {
			dx = -dx;
			dy = -dy;
			interact2(n, p);
		}
	}

	bool interact2(Drawable p, Drawable n) {
		if (auto pp = cast(Patient)p) {
			if (auto nn = cast(Patient)n) {
				interact(pp, nn);
				return true;
			}
			if (auto nn = cast(Explosion)n) {
				interact(pp, nn);
				return true;
			}
			if (auto nn = cast(Diarrhoea)n) {
				interact(pp, nn);
				return true;
			}
		} else if (auto pp = cast(Diarrhoea)p) {
			if (auto nn = cast(Diarrhoea)n) {
				interact(pp, nn);
				return true;
			}
		}
		return false;
	}

	void interact(Patient p, Patient n) {
		if (dist < InteractionDistances.patientAvoidance) {
			float adjustment = (InteractionDistances.patientAvoidance - dist) * 0.5;
			if (dist < 0.01) {
				float a = uniform!"[)"(0, 2*3.14159);
				dx = adjustment * sin(a);
				dy = adjustment * cos(a);
			} else {
				adjustment /= dist;
				dx *= adjustment;
				dy *= adjustment;
			}
			p.x += dx;
			p.y += dy;
			n.x -= dx;
			n.y -= dy;
			interact2(p, n);
			interact2(n, p);
		}
	}

	void interact2(Patient a, Patient b) {
		//If a is sneezing or blobby, infect b with what a has.
		if (a.currentSneezeCounter || a.blobbiness) {
			if (a.constipation && !b.contagiousConstipationInteractionCounter) {
				b.constipation = clamp(b.constipation + sgn(a.constipation), b.minConstipation, b.maxConstipation);
				b.contagiousConstipationInteractionCounter = contagionInteractionInterval;
			}
			if (a.temperature && !b.contagiousTemperatureInteractionCounter) {
				b.temperature = clamp(b.temperature + sgn(a.temperature), b.minTemperature, b.maxTemperature);
				b.contagiousTemperatureInteractionCounter = contagionInteractionInterval;
			}
		}
		//Blobbiness allows infection itself to be transmitted.
		if (a.blobbiness && !b.contagiousInfectionInteractionCounter) {
			if (b.infection < b.maxInfection) b.infection++;
			b.contagiousInfectionInteractionCounter = contagionInteractionInterval;
		}
	}

	void interact(Patient p, Explosion n) {
		if (n.isPrimary && n.lifetime < explosionLifetime - explosionTransmitDelay && dist < InteractionDistances.patientExplosion) {
			p.destroyedDuringInteraction = true;
			theGame.awardPoints(pointsForKilling);
			auto explosion = new Explosion();
			explosion.isPrimary = false;
			explosion.x = p.x;
			explosion.y = p.y;
			theGame.explosions ~= explosion;
			explosion.playSample(sfx.burst, 0.5);
		}
	}

	void interact(Patient p, Diarrhoea n) {
		if (dist < InteractionDistances.patientDiarrhoea) {
			//Very feverish (or on fire) patients set fire to diarrhoea
			if ((p.temperature == p.maxTemperature || p.fireCounter) && !n.fireLifetime) {
				n.fireLifetime = diarrhoeaFireLifetime;
			}
			if (n.fireLifetime) {
				//Patients with chills can put the fire out.
				if (p.temperature < 0 && !n.preventFireSpread) {
					n.preventFireSpread = true;
				}
				if (p.diarrhoeaInteractionCounter == 0) {
					p.diarrhoeaInteractionCounter = diarrhoeaInteractionInterval;
					//Patients not already feverish move towards the hot end of the scale.
					if (p.temperature < p.maxTemperature) {
						p.temperature++;
					}
					//Patients who don't have chills are set fire to and killed
					//(provided the fire is mature enough - we may have only just started it ourselves).
					if (p.temperature > 0 && !p.fireCounter && n.fireLifetime <= diarrhoeaFireLifetime - diarrhoeaPatientBurnDelay) {
						p.fireCounter = patientFireLifetime;
					}
				}
			}
		}
	}

	void interact(Diarrhoea p, Diarrhoea n) {
		if (dist < InteractionDistances.diarrhoeaFireTransmission) {
			int transmissionTime = cast(int)round(diarrhoeaFireTransmissionTime * dist / InteractionDistances.diarrhoeaFireTransmission);
			interact2(p, n, transmissionTime);
			interact2(n, p, transmissionTime);
		}
	}

	void interact2(Diarrhoea a, Diarrhoea b, int transmissionTime) {
		//If a is on fire, transmit the fire to b after a time.
		if (a.fireLifetime && !a.preventFireSpread) {
			auto otherLifetime = a.fireLifetime + transmissionTime;
			if (otherLifetime <= diarrhoeaFireLifetime && (!b.fireLifetime || otherLifetime < b.fireLifetime)) {
				b.fireLifetime = otherLifetime;
			}
		}
	}
}
