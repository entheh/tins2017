import std.random;
import std.math;
import allegro5.allegro_audio;
import main;

abstract class Drawable {
	float x, y;
	bool destroyedDuringInteraction;

	abstract void draw();

	void playSample(ALLEGRO_SAMPLE* sample, float gain) {
		auto pan = x*(2.0/800) - 1;
		enum pitchVar = 0.2;
		auto speed = exp(uniform!"[]"(-pitchVar, pitchVar));
		al_play_sample(sample, gain, pan, speed, ALLEGRO_PLAYMODE.ALLEGRO_PLAYMODE_ONCE, null);
	}

	void addFireSfx(float gain) {
		float t = x*gain/800;
		sfx.fireLeftGain += gain-t;
		sfx.fireRightGain += t;
	}
}
